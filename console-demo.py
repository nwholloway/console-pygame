#! /usr/bin/python3

import signal
import sys
import time

import pygame

COLON_RECTS = [(0, 80, 20, 20), (0, 140, 20, 20)]

SEGMENT_POLYGONS = [
    [(15, 10), (25, 0), (105, 0), (115, 10), (105, 20), (25, 20)],
    [(110, 25), (120, 15), (130, 25), (130, 100), (120, 110), (110, 100)],
    [(110, 130), (120, 120), (130, 130), (130, 205), (120, 215), (110, 205)],
    [(15, 220), (25, 210), (105, 210), (115, 220), (105, 230), (25, 230)],
    [(0, 130), (10, 120), (20, 130), (20, 205), (10, 215), (0, 205)],
    [(0, 25), (10, 15), (20, 25), (20, 100), (10, 110), (0, 100)],
    [(15, 115), (25, 105), (105, 105), (115, 115), (105, 125), (25, 125)],
]

SEGMENTS = {
    0: [0, 1, 2, 3, 4, 5],
    1: [1, 2],
    2: [0, 1, 3, 4, 6],
    3: [0, 1, 2, 3, 6],
    4: [1, 2, 5, 6],
    5: [0, 2, 3, 5, 6],
    6: [0, 2, 3, 4, 5, 6],
    7: [0, 1, 2],
    8: [0, 1, 2, 3, 4, 5, 6],
    9: [0, 1, 2, 5, 6],
}


def main():
    signal.signal(signal.SIGTERM, lambda signum, frame: sys.exit())

    try:
        pygame.display.init()
        pygame.mouse.set_visible(False)

        screen = pygame.display.set_mode((1280, 720))
        clock = pygame.time.Clock()

        while True:
            events = pygame.event.get()
            if list(filter(lambda e: e.type == pygame.QUIT, events)):
                return

            draw(screen)

            pygame.display.flip()
            clock.tick(10)
    except KeyboardInterrupt:
        pass
    finally:
        pygame.display.quit()


def draw(screen):
    hour1 = screen.subsurface((120, 240), (130, 230))
    hour2 = screen.subsurface((270, 240), (130, 230))
    colon1 = screen.subsurface((420, 240), (20, 230))
    min1 = screen.subsurface((460, 240), (130, 230))
    min2 = screen.subsurface((610, 240), (130, 230))
    colon2 = screen.subsurface((760, 240), (20, 230))
    sec1 = screen.subsurface((800, 240), (130, 230))
    sec2 = screen.subsurface((950, 240), (130, 230))

    bg = pygame.Color('darkslategray')
    fg = pygame.Color('green2')

    screen.fill(bg)

    tm = time.localtime()
    tm_hour = tm.tm_hour % 12

    if tm_hour > 9:
        draw_digit(hour1, fg, tm_hour // 10)
    draw_digit(hour2, fg, tm_hour % 10)
    draw_digit(min1, fg, tm.tm_min // 10)
    draw_digit(min2, fg, tm.tm_min % 10)
    draw_digit(sec1, fg, tm.tm_sec // 10)
    draw_digit(sec2, fg, tm.tm_sec % 10)
    draw_colon(colon1, fg)
    draw_colon(colon2, fg)


def draw_colon(surface, color):
    for rect in COLON_RECTS:
        pygame.draw.rect(surface, color, rect, 0)


def draw_digit(surface, color, digit):
    for segment in SEGMENTS.get(digit, []):
        pygame.draw.polygon(surface, color, SEGMENT_POLYGONS[segment], 0)


if __name__ == '__main__':
    main()
