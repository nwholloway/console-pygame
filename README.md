# Console Pygame Demo Application

This is a demonstration application to show how a Pygame application can
be launched when the Raspberry Pi boots, so your Pi and TV/monitor can
be used to display whatever you need it to without manual intervention.

The sample application is just a digital clock, but the permissions are
set so it could access the serial port, the I2C and SPI buses, and the
GPIO pins.  Pretty much anything you might want on a Pi.

The reason for using Pygame is that it uses SDL for its graphics, so it
can either run directly on the console (which is how it is run at boot),
or when you run from desktop GUI it appears as a windowed application
(which is great for development).

## Installation

The script is run as a `systemd` service, so it will start automatically
when the Pi boots.  The service runs as user `nobody` with groups `video`,
`input`, `dialout`, `spi`, `i2c`, and `gpio`.  These groups should be
sufficient for most applications running on the Pi.

The initial installation is performed using these commands:
```
sudo apt install -y python3-pygame
sudo install -t /usr/local/lib console-demo.py
sudo install -t /etc/systemd/system console-pygame.service
sudo systemctl daemon_reload
sudo systemctl enable console-pygame
sudo systemctl start console-pygame
```

## Monitoring

To check on the status of the service:
```
systemctl status console-pygame
```

To see the full output (including any errors):

```
journalctl -u console-pygame
```

## Updating

To update the installed script:
```
sudo install -t /usr/local/lib console-demo.py
sudo systemctl restart console-pygame
```
